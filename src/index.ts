/*
 * Public API Surface of stasis
 */

export * from './lib/stasis.service';
export * from './lib/stasis.module';
